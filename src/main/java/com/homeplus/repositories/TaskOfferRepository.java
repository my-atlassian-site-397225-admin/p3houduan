package com.homeplus.repositories;

import com.homeplus.models.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface TaskOfferRepository extends JpaRepository<TaskOfferEntity, Long> {

    @Query("select o from TaskOfferEntity as o where o.taskEntity = :taskEntity")
    Optional<TaskOfferEntity> findOffersByTask(TaskEntity taskEntity);

    @Query("select o from TaskOfferEntity as o where o.taskEntity = :taskEntity and o.taskerEntity = :taskerEntity and o.offer_status != 'CANCELED'")
    Optional<TaskOfferEntity> findOfferByTaskAndTasker(TaskEntity taskEntity, TaskerEntity taskerEntity);

    @Transactional
    @Modifying
    @Query("update TaskOfferEntity as o set o.offer_status = 'CANCELED' where o.id = :id")
    void cancelById(Long id);

    @Transactional
    @Modifying
    @Query("update TaskOfferEntity as o " +
            "set o.offer_status = :status, " +
            "o.reply_message = :reply_msg, " +
            "o.updated_time = :now " +
            "where o.id = :id")
    void acceptOrRejectOffer(Long id, String reply_msg, TaskOfferStatus status, OffsetDateTime now);

    @Query("select o from TaskOfferEntity as o where o.taskEntity = :taskEntity and o.offer_status = 'PENDING'")
    Optional<TaskOfferEntity> getPendingOffers(TaskEntity taskEntity);

    @Query("select o from TaskOfferEntity as o where o.taskerEntity = :taskerEntity and o.offer_status = 'ACCEPTED'")
    Optional<TaskOfferEntity> getAcceptedOffers(TaskerEntity taskerEntity);
}
